package ru.t1.volkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}

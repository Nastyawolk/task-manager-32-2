package ru.t1.volkova.tm.dto.request.user;

import lombok.NoArgsConstructor;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class UserProfileRequest extends AbstractUserRequest {

}

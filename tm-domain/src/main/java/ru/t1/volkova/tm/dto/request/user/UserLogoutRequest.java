package ru.t1.volkova.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public final class UserLogoutRequest extends AbstractUserRequest {


}

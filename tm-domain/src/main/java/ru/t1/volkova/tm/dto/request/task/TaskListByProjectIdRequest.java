package ru.t1.volkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.TaskSort;

@Getter
@Setter
public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable final TaskSort sort, @Nullable final String projectId) {
        this.sort = sort;
        this.projectId = projectId;
    }

}

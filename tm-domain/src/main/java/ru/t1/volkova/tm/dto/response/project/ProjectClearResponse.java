package ru.t1.volkova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectClearResponse extends AbstractProjectResponse {

    public ProjectClearResponse(@Nullable final Project project) {
        super(project);
    }

}

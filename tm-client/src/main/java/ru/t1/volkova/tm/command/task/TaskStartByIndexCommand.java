package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Start task status by index.";

    @NotNull
    private static final String NAME = "task-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(index, Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}

package ru.t1.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.data.DataJsonLoadFasterXmlRequest;
import ru.t1.volkova.tm.enumerated.Role;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from json file";

    @NotNull
    public static final String NAME = "data-load-json-faster";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        serviceLocator.getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

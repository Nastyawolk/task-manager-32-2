package ru.t1.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.data.DataXmlSaveJaxBRequest;
import ru.t1.volkova.tm.enumerated.Role;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in XML file";

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        serviceLocator.getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

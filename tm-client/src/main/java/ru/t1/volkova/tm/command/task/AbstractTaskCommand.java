package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            String print = index++ + ". " +
                    task.getName() + " | " +
                    task.getId() + " | " +
                    task.getStatus();
            System.out.println(print);
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
